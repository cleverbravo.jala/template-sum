﻿using sum_max;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test.sum_max
{
    public class TestSolution
    {
        [Theory]
        [InlineData(new int[] { 2, 3 }, new int[] { 1, 2, 3, 0 })]
        [InlineData(new int[] { 8, 9 }, new int[] { 4, 3, 9, 1, 2, 5, 8, 0 })]
        [InlineData(new int[] { 79, 84 }, new int[] { 57, 10, 29, 55, 27, 19, 79, 84, 4 })]
        [InlineData(new int[] { 66, 99 }, new int[] { 99, 35, 23, 1, 66 })]
        [InlineData(new int[] { 79, 85 }, new int[] { 73, 49, 28, 79, 85 })]
        [InlineData(new int[] { 78, 94 }, new int[] { 30, -8, 4, 78, 94, 28, 75, -9 })]
        [InlineData(new int[] { 89, 97 }, new int[] { 15, -9, 97, 9, 3, 89 })]
        [InlineData(new int[] { 51, 56 }, new int[] { 42, 51, 30, 56, 37, 17 })]
        [InlineData(new int[] { 49, 75 }, new int[] { 49, 15, 37, 31, 42, 75, 48 })]
        [InlineData(new int[] { 61, 89 }, new int[] { 40, 34, 35, 61, 89, 47, 36 })]
        [InlineData(new int[] { 88, 91 }, new int[] { 13, 33, 60, 46, 88, 91, 60, 16, 71 })]
        [InlineData(new int[] { 79, 80 }, new int[] { 80, 30, 26, 20, 55, 79 })]
        public void Test1(int[] expected, int[] n)
        {
            Solution solution = new();

            Stopwatch stopwatch = Stopwatch.StartNew();
            var result = solution.sum(n);
            stopwatch.Stop();

            File.AppendAllText("results.csv", $"{string.Join(";", n)} - {stopwatch.Elapsed}{Environment.NewLine}");
            Assert.Equal(expected, result);
        }
    }
}
